// Gruntfile.js

// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  grunt.initConfig({

    // all of our configuration will go here
    sass: {
      default: {
        files: [{
          expand: true,
          cwd: 'sass',
          src: ['**/*.scss'],
          dest: 'css',
          ext: '.css'
        }]
      }
    },

    shell: {
      options: {
        stderr: false,
      },
      drushcc: {
        command: 'drush cache-clear render && drush cache-clear theme-registry'
      }
    },

    watch: {
      sass: {
        files: ['sass/**/*.scss'],
        tasks: ['sass']
      },
      less: {
        files: ['less/**/*.less'],
        tasks: ['less'],
      },
      twig: {
        files: ['templates/**/*.twig'],
        tasks: ['shell'],
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [ 'css/*.css', 'templates/**/*.twig' ]
        },
        options: {
          watchTask: true,
          injectChanges: false,
          reloadDelay: 3000,
          proxy: "localhost/sandbox"
        }
      }
    }
  });

  grunt.registerTask('default', ['sass', 'browserSync', 'watch']);

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // we can only load these if they are in our package.json
  // make sure you have run npm install so our app can find these
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-browser-sync');
};
